<?php
// web/index.php
 
use Symfony\Component\HttpFoundation\Request;
 
require_once __DIR__.'/../vendor/autoload.php';
 
$app = new Silex\Application();
 
$app->get('/', function (Request $request) use ($app) {
	return $request->server->get("HTTP_X_FORWARDED_FOR");
});
 
$app->run();
